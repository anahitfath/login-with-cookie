import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import AuthApi from "./AuthApi";
import Cookies from "js-cookie";

function App() {
  const [auth, setAuth] = useState(false);
  const readCookie = () => {
    const user = Cookies.get("user");
    if (user) {
      setAuth(true);
    }
  };

  useEffect(() => {
    readCookie();
  }, []);

  return (
    <div>
      <AuthApi.Provider value={{ auth, setAuth }}>
        <Router>
          <Routes />
        </Router>
      </AuthApi.Provider>
    </div>
  );
}

const Login = () => {
  const [token, setToken] = useState("");
  const [errorText, setErrorText] = useState("");
  const Auth = React.useContext(AuthApi);
  const handleSubmit = (event) => {
    event.preventDefault();
  };
  const handleOnClick = (event) => {
    const myToken = "test";
    if (token === myToken) {
      Auth.setAuth(true);
      Cookies.set("user", "loginTrue");
    } else {
      setErrorText("Please enter correct username");
      setToken("");
    }
  };

  const handleChange = (event) => {
    setToken(event.target.value);
  };
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <img src={require("./Assets/login-logo.jpg")} />
        <h3>Welcome To This Website</h3>
        <p>Enter "test" keyword as username to login</p>
        <input
          value={token}
          onChange={handleChange}
          type="text"
          placeholder="Enter your username"
        />

        <button onClick={handleOnClick}>Login</button>
        <div className="error-text">{errorText}</div>
      </form>
    </div>
  );
};

const Dashboard = () => {
  const Auth = React.useContext(AuthApi);

  const handleOnClick = () => {
    Auth.setAuth(false);
    Cookies.remove("user");
  };
  return (
    <div className="welcome-page">
      <img src={require("./Assets/welcome-image.jpg")} />
      <h3>Hello</h3>
      <p>You are logged in</p>
      <h5>To logout click this button</h5>
      <button onClick={handleOnClick}>Logout</button>
    </div>
  );
};



const Routes = () => {
  const Auth = React.useContext(AuthApi);
  return (
    <Switch>
      <ProtectedLogin path="/login" component={Login} auth={Auth.auth} />

      <ProtectedRoute
        path="/dashboard"
        auth={Auth.auth}
        component={Dashboard}
      />
    </Switch>
  );
};

const ProtectedRoute = ({ auth, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={() => (auth ? <Component /> : <Redirect to="/login" />)}
    />
  );
};

const ProtectedLogin = ({ auth, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={() => (!auth ? <Component /> : <Redirect to="/dashboard" />)}
    />
  );
};

export default App;
